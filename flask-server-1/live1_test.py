from flask import Flask, render_template,send_file
from flask_socketio import SocketIO, emit
import pymongo
from bson.objectid import ObjectId
#------------------------------
# LOCAL DATABASE INITIALIZE
#------------------------------
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["kpcl"]
joblist_db = mydb["testbed1"]
joblist_db.drop()
#---------------------
#Queries
#---------------------
def return_job_serial_nos():
    serial_number =[]
    print("------------------------------")
    for x in joblist_db.find({}, {"_id": 1}):
        serial_number.append(str(x["_id"]))
        print(x["_id"])
    print("------------------------------")
    serial_dict ={"serial_nos":serial_number}
    return  serial_dict
def return_job_details(serial_id):
    myquery = {"_id": ObjectId(serial_id)}

    mydoc = joblist_db.find(myquery)
    for x in mydoc:
        print(x)
    return  x
def return_job_details_jobtype(jobtype):
    myquery = {"jobtype": jobtype}

    mydoc = joblist_db.find(myquery).sort("_id",pymongo.ASCENDING)
    all_x=[]
    for x in mydoc:
        #print(x)
        x["_id"]=str(x["_id"])
        all_x.append(x)
    return  all_x
#----------------------------
from pymodbus.client.sync import ModbusSerialClient as Modbusclient
import time
import math
from ctypes import *
'''import RPi.GPIO as GPIO
import time
import time
import Adafruit_ADS1x15'''

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder


from bson import ObjectId



def live1_thread(msg,socketio):
    print("----------------------------IN LIVE2THREAD------------------------------")
    print(msg)
    print('success in live2')
    socketio.emit('message',{'Success':"Live2"})